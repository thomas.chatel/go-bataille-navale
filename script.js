const line = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
const host = getParameterByName('host')
const target = getParameterByName('target')

document.addEventListener('DOMContentLoaded', () => {
    const url = `http://localhost:${host}/board?focus=${target}`
    fetch(url)
        .then(res => {
            return res.json()
        })
        .then(res => {
            displayBoard(res)
        })
        .catch(error => {
            console.log('Une erreur est survenue')
        })
})

function displayBoard(dataBoard) {
    const board = document.getElementById('board')

    for (let i = 0; i < dataBoard.length; i++) {
        for (let j = 0; j < dataBoard[i].length; j++) {
            const caseBoard = board.querySelector('#' + line[i] + j)
            caseBoard.addEventListener('click', () => attackPosition(i, j))
            if (dataBoard[i][j] === 0) {
                caseBoard.classList.add('empty')
            } else if (dataBoard[i][j] === 1) {
                caseBoard.classList.add('inWater')
            } else if (dataBoard[i][j] === 2) {
                caseBoard.classList.add('hitted')
            }
        }
    }
}

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function attackPosition(lineToAttack, columnToAttack) {
    const board = document.getElementById('board')
    const errorText = document.getElementById('errorHit')
    const data = {
        focus: target,
        line: lineToAttack,
        column: columnToAttack
    }

    fetch(`http://localhost:${host}/hit`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        body: new URLSearchParams(data)
    }).then(res => {
        return res.json()
    }).then(res => {
        const caseBoard = board.querySelector('#' + line[lineToAttack] + columnToAttack)
        if (res === 'Missed') {
            caseBoard.classList.add('inWater')
        } else {
            caseBoard.classList.add('hitted')
            checkBoatStatus()
        }
        errorText.classList.add('hidden')
    }).catch(() => {
            errorText.classList.remove('hidden')
        }
    )
}

function checkBoatStatus() {
    const gameStatus = document.getElementById('gameStatus')
    fetch(`http://localhost:${host}/boats?focus=${target}`)
        .then(res => {
            return res.json()
        })
        .then(res => {
            if (res !== 0){
                gameStatus.innerText = `Il reste ${res} bateaux.`
            } else {
                gameStatus.innerText = `Vous avez gagné.`
            }
        })
        .catch(error => {
            console.log('Une erreur est survenue')
        })
}