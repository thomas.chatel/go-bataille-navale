package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"
)

var EMPTY int = 0
var MISSED int = 1
var TOUCHED int = 2
var BOAT int = 3

var ROWDIRECTION = 0
var COLUMNDIRECTION = 1

var TAB_LETTERS = [10]string{"A","B","C","D","E","F","G", "H","I","J"}



var playersMatrix = make(map[string][10][10]int)
var target string

func main()  {
	port := ":" + os.Args[1]

	go startServer(port)

	fmt.Println("\n***** BIENVENUE SUR NAVAL BATTLE ULTIMATE *****\n")
	for true {
		var choice int
		fmt.Println("[1] Rejoindre une partie")
		fmt.Println("[2] Créer une partie")
		fmt.Println("[3] Quitter")
		fmt.Scanf("%d", &choice)

		switch choice {
			case 1:
				joinGame()
			case 2:
				createGame()
			case 3:
				os.Exit(0)
			default:
				fmt.Println("Entrée invalide")
		}
	}
}

func boardHandler(w http.ResponseWriter, req *http.Request) {
	enableCors(&w)
	if req.Method == http.MethodGet {
		focus := req.URL.Query().Get("focus")
		if _, exist := playersMatrix[focus]; !exist {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("404 - Not found"))
			return
		}
		var visibleMatrix [10][10]int
		for l := 0; l < 10; l++ {
			for c := 0; c < 10; c++ {
				if playersMatrix[focus][l][c]/10 == BOAT {
					visibleMatrix[l][c] = 0
				} else {
					visibleMatrix[l][c] = playersMatrix[focus][l][c]
				}
			}
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(visibleMatrix)

	} else {
		fmt.Fprintf(w, "Requête non prise en charge")
	}
}

func boatsHandler(w http.ResponseWriter, req *http.Request) {
	enableCors(&w)
	if req.Method == http.MethodGet {
		focus := req.URL.Query().Get("focus")
		if _, exist := playersMatrix[focus]; !exist {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("404 - Not found"))
			return
		}

		nbBoats := 0
		var boats = make(map[int]bool)
		for i := 0; i < 10; i++ {
			for j := 0; j < 10; j++ {
				if _, exist := boats[playersMatrix[focus][i][j]%10]; !exist && playersMatrix[focus][i][j]/10 == 3 {
					boats[playersMatrix[focus][i][j]%10] = true
					nbBoats += 1
				}
			}
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(nbBoats)

	} else {
		fmt.Fprintf(w, "Requête non prise en charge")
	}
}

func hitHandler(w http.ResponseWriter, req *http.Request) {
	enableCors(&w)
	if req.Method == http.MethodPost {
		if err := req.ParseForm(); err != nil || req.PostForm["line"] == nil || req.PostForm["column"] == nil || req.PostForm["focus"] == nil  {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("400 - Bad request"))
			return
		}

		focus := req.PostForm["focus"][0]
		if _, exist := playersMatrix[focus]; !exist {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("404 - Not found"))
			return
		}

		line, errl := strconv.Atoi(req.PostForm["line"][0])
		column, errc := strconv.Atoi(req.PostForm["column"][0])
		if errl != nil || errc != nil || line > 9 || column > 9 || playersMatrix[focus][line][column] == TOUCHED || playersMatrix[focus][line][column] == MISSED {
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("403 - Forbidden"))
			return
		}
		matrix := playersMatrix[focus]

	    w.Header().Set("Content-Type", "application/json")
		if matrix[line][column]/10 == BOAT {
			matrix[line][column] = TOUCHED
			json.NewEncoder(w).Encode("Touched")
		} else {
			matrix[line][column] = MISSED
			json.NewEncoder(w).Encode("Missed")
		}
		playersMatrix[focus] = matrix
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("405 - Méthode non prise en charge"))
	}
}

func createRoomHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodGet {
		if _, exist := playersMatrix[os.Args[1]]; exist {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("403 - Forbidden"))
			return
		}
		playersMatrix[os.Args[1]] = generateMatrix()
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("200 - OK!"))
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("405 - Méthode non prise en charge"))
	}
}

func joinRoomHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodGet {

		if err := req.ParseForm(); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("400 - Bad request"))
			return
		}
		_, partyExist := playersMatrix[os.Args[1]]
		_, alreadyJoined := playersMatrix[req.URL.Query().Get("player")]
		if !partyExist || alreadyJoined {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("403 - Forbidden"))
			return
		}
		playersMatrix[req.URL.Query().Get("player")] = generateMatrix()
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("200 - OK!"))
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("405 - Méthode non prise en charge"))
	}
}

func playersRoomHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodGet {
		var players []string
		for player := range playersMatrix {
			players = append(players, player)
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(players)
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("405 - Méthode non prise en charge"))
	}
}

func leaveRoomHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodGet {
		delete(playersMatrix, req.URL.Query().Get("player"))
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("405 - Méthode non prise en charge"))
	}
}

func startServer(port string) {
	http.HandleFunc("/board", boardHandler)
	http.HandleFunc("/boats", boatsHandler)
	http.HandleFunc("/hit", hitHandler)
	http.HandleFunc("/room/create", createRoomHandler)
	http.HandleFunc("/room/join", joinRoomHandler)
	http.HandleFunc("/room/players", playersRoomHandler)
	http.HandleFunc("/room/leave", leaveRoomHandler)
	http.ListenAndServe(port, nil)
}

func generateMatrix() [10][10]int {
	var remainingsBoats = make(map[int]int)
	remainingsBoats[2] = 1
	remainingsBoats[3] = 2
	remainingsBoats[4] = 1
	remainingsBoats[5] = 1
	totalRemaining := 5

	var matrix [10][10]int

	for i := 0; i < len(matrix); i++ {
		for j := 0; j < len(matrix[i]); j++ {
			matrix[i][j] = EMPTY
		}
	}

	for totalRemaining > 0 {
		l := random(0, 9)
		c := random(0, 9)
		lenBoat := random(2, 5)
		direction := random(ROWDIRECTION, COLUMNDIRECTION)
		for !boatCanBePlaced(matrix, remainingsBoats, l, c, lenBoat, direction) {
			l = random(0, 9)
			c = random(0, 9)
			lenBoat = random(2, 5)
			direction = random(ROWDIRECTION, COLUMNDIRECTION)
		}

		for i := 0; i < lenBoat; i++ {
			if direction == ROWDIRECTION {
				matrix[l][c+i] = BOAT*10 + totalRemaining
			} else {
				matrix[l+i][c] = BOAT*10 + totalRemaining
			}
		}
		remainingsBoats[lenBoat] -= 1
		totalRemaining -= 1
	}
	return matrix

}

func boatCanBePlaced(matrix [10][10]int, remainingBoats map[int]int, l int, c int, lenBoat int, direction int) bool {
	if remainingBoats[lenBoat] == 0 {
		return false
	}
	if (direction == ROWDIRECTION && c+lenBoat > 9) || (direction == COLUMNDIRECTION && l+lenBoat > 9) {
		return false
	}
	for i := 0; i < lenBoat; i++ {
		if (direction == ROWDIRECTION && matrix[l][c+i] != EMPTY) || (direction == COLUMNDIRECTION && matrix[l+i][c] != EMPTY) {
			return false
		}
	}
	return true
}

func random(a, b int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(b+1-a) + a
}

func joinGame()  {
	var port string
	fmt.Println("Quelle partie voulez-vous rejoindre ?")
	fmt.Scanf("%s", &port)
	target = port
	res, err := http.Get("http://localhost:" + port + "/room/join?player=" + os.Args[1])
	if err != nil {
		fmt.Println("La partie n'a pas été trouvée")
		return
	}
	defer res.Body.Close()

	if res.StatusCode == http.StatusOK {
		lobby()
	} else {
	    fmt.Println("La partie n'a pas été trouvée")
	}
}

func createGame() {
	target = os.Args[1]
	res, err := http.Get("http://localhost:" + os.Args[1] + "/room/create")
	if err != nil {
		fmt.Println("Erreur lors de la création de la partie")
		return
	}
	defer res.Body.Close()

	if res.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(res.Body)
		if err != nil {
			fmt.Println("Erreur lors de la création de la partie")
			return
		}
		bodyString := string(bodyBytes)
		fmt.Println(bodyString)
		lobby()
	} else {
		fmt.Println("Erreur lors de la création de la partie")
	}
}

func lobby() {
	for true {
		var choice int
		fmt.Println("\n[1] Lister les joueurs")
		fmt.Println("[2] Visionner le tableau d'un joueur")
		fmt.Println("[3] Attaquer un joueur")
		fmt.Println("[4] Voir les bateaux restants d'un joueur")
		fmt.Println("[5] Quitter la partie")
		fmt.Scanf("%d", &choice)

		switch choice {
            case 1:
                listPlayers()
            case 2:
                var focusPlayer string
                fmt.Print("Notez son id : ")
                fmt.Scanf("%v", &focusPlayer)
                getAndShowPlayerLevel(focusPlayer)
            case 3:
                var focusPlayer string
                fmt.Print("Notez son id : ")
                fmt.Scanf("%v", &focusPlayer)

                var addressToHit string
                fmt.Print("Notez l'adresse à attaquer (ex : B4) : ")
                fmt.Scanf("%v", &addressToHit)

                var line int = -1

                for i, letter := range TAB_LETTERS {
                    if letter == string(addressToHit[0]) {
                        line = i
                        break
                    }
                }
                if (line == -1) {
                    fmt.Println("Vous avez mal rentré l'adresse")
                    break
                }
                column, err := strconv.Atoi(string(addressToHit[1]))
                if err != nil {
                    fmt.Println("Vous avez mal rentré l'adresse")
                    break
                }
                hitPlayer(focusPlayer, line, column)
            case 4:
                var focusPlayer string
                fmt.Print("Notez son id : ")
                fmt.Scanf("%v", &focusPlayer)

                countBoatsPlayer(focusPlayer)
            case 5:
				http.Get("http://localhost:" + target + "/room/leave?player=" + os.Args[1])
                return
            default:
                fmt.Println("Entrée invalide")
            }
	}
}

func countBoatsPlayer(focusPlayer string) {
    res, err := http.Get("http://localhost:" + target + "/boats?focus="+focusPlayer)
    if err != nil {
    	fmt.Println("Erreur lors de la récupération du nombre de bateaux de "+focusPlayer)
    	return
    }
    defer res.Body.Close()

    if res.StatusCode == http.StatusOK {
    	bodyBytes, err := ioutil.ReadAll(res.Body)
    	if err != nil {
    		fmt.Println("Erreur lors de la récupération du nombre de bateaux de "+focusPlayer)
    		return
    	}

    	var nbBoats int
    	json.Unmarshal(bodyBytes, &nbBoats)

    	fmt.Printf("\n%v a %v bateaux\n", focusPlayer, nbBoats)
    } else {
    	fmt.Println("Erreur lors de la récupération du nombre de bateaux de "+focusPlayer)
    }
}

func hitPlayer(focusPlayer string, line int, column int) {
    data := url.Values{}
    data.Set("focus", focusPlayer)
    data.Set("line", strconv.Itoa(line))
    data.Set("column", strconv.Itoa(column))

    res, err := http.PostForm("http://localhost:" + target + "/hit", data)
    if err != nil {
   	    log.Fatal(err)
    	fmt.Println("Erreur lors de l'attaque contre "+focusPlayer)
    	return
    }
    defer res.Body.Close()

    if res.StatusCode == http.StatusOK {
    	bodyBytes, err := ioutil.ReadAll(res.Body)
        if err != nil {
        	fmt.Println("Erreur lors de l'affichage du tableau de "+focusPlayer)
        	return
        }

        var res string
        json.Unmarshal(bodyBytes, &res)

        if res == "Touched" {
            fmt.Println("\nCible touchée!")
        } else {
            fmt.Println("\nCible manquée")
        }

    } else {
    	fmt.Println("\nErreur lors de l'attaque contre "+focusPlayer)
    }
}

func getAndShowPlayerLevel(focusPlayer string) {
	res, err := http.Get("http://localhost:" + target + "/board?focus="+focusPlayer)
	if err != nil {
		fmt.Println("Erreur lors de l'affichage du tableau de "+focusPlayer)
		return
	}
	defer res.Body.Close()

	if res.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(res.Body)
		if err != nil {
			fmt.Println("Erreur lors de l'affichage du tableau de "+focusPlayer)
			return
		}

		var level [10][10]int
		json.Unmarshal(bodyBytes, &level)
		showMatrix(level)
	} else {
		fmt.Println("Erreur lors de l'affichage du tableau de "+focusPlayer)
	}
}

func showMatrix(matrix [10][10]int) {
    fmt.Println("   | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9\n")

	for i := 0; i < len(matrix); i++ {
		fmt.Printf(TAB_LETTERS[i]+" ");
		for j := 0; j < len(matrix[i]); j++ {
		    fmt.Printf(" | %v", matrix[i][j])
		}
		fmt.Printf("\n")
	}
}

func listPlayers() {
	res, err := http.Get("http://localhost:" + target + "/room/players")
	if err != nil {
		fmt.Println("Erreur lors du listing des joueurs")
		return
	}
	defer res.Body.Close()

	if res.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(res.Body)
		if err != nil {
			fmt.Println("Erreur lors du listing des joueurs")
			return
		}
		var players []string
        json.Unmarshal(bodyBytes, &players)
        fmt.Println("Les joueurs : "+strings.Join(players, " | "))
	} else {
		fmt.Println("Erreur lors du listing des joueurs")
	}
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func pos(arr []string, value string) int {
	for p, v := range arr {
		if v == value {
			return p
		}
	}
	return -1
}